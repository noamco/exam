@extends('layouts.app')
@section('content')
<h1>Edit Existing Task</h1>
<form method = 'POST' action = "{{action('TaskController@update' , $task->id)}}">
@method('PATCH')
@csrf
<div class = "form-group">
<label for = "title">Task to edit:</label>
<input type = "text" class= "form-control" name = "title" value = "{{$task->title}}">
</div>

<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit" value = "Save">
</div>
</form>


@endsection