@extends('layouts.app')
@section('content')
<h1>Put your car up for sale</h1>
<form method = 'POST' action ="{{action('CustomerController@store')}}">
@csrf
<div class = "form-group">
<label for = "name">Name</label>
<input type = "text" class= "form-control" name = "name">

<label for = "email">E-mail Address</label>
<input type = "email" class= "form-control" name = "email">
<label for = "phone">Phone Number</label>
<input type = "phone" class= "form-control" name = "phone">
</div>

<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit" value = "Save">
</div>

<!-- add following if asked for verification -->
@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


</form>
@endsection