@yield('content')
@extends('layouts.app')
@section('content')


<h1>Customer List</h1>

<h4> Hello {{auth()->user()->name}},<br></h4>
<h5>Your role is: {{auth()->user()->role}}</h5><br><br>

<h3><a href = "{{route('customers.create')}}">Create new customer </a></h3>


<table class="table">
<tr>
    <th>Name</th>
    <th>E-mail</th>
    <th>Phone</th>
    <th>Edit</th>
    <th>Delete</th>
    @can('manager')<th>Deal Closed</th>@endcan


</tr>

@foreach($customers as $customer)

   @if(auth()->user()->id == $customer->user_id)
   <tr style="font-weight: bold;">
   @else
   <tr>
    @endif


@if ($customer->status==1)
<td> <span style="color: green;"> {{$customer->name}} (Responsible user: {{$customer->user->name}}) </td> 
@else
<td> {{$customer->name}}  (Responsible user: {{$customer->user->name}}) </td> 
@endif

<td> {{$customer->email}} </td>
<td> {{$customer->phone}}</td> 




<td><a href = "{{route('customers.edit',$customer->id)}}">  Edit</a></td>
<td>@can('manager')<a href="{{route('delete', $customer->id)}}">Delete</a>@endcan
    @can('salesrep') <h4>Delete</h4> @endcan
</td>
<td>
@can('manager')
    @if ($customer->status==1)
           <h4></h4>
       @else
       <a href="{{route('deal', $customer->id)}}">Deal closed</a>
       @endif</td>
@endcan

</td>
</tr>

@endforeach
</table>
<br><br>


<!-- @if (session('alert'))
    <div class="alert alert-success">
        {{ session('alert') }}
    </div>
@endif -->

<script>
  var msg = '{{Session::get('alert')}}';
  var exist = '{{Session::has('alert')}}';
  if(exist){
    alert(msg);
  }
</script>





@endsection
