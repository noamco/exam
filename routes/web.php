<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('customers','CustomerController')->middleware('auth');
Route::get('customers/delete/{id}', 'CustomerController@destroy')->name('delete');
Route::get('customers/deal/{id}', 'CustomerController@deal')->name('deal');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
