<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;//verification
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        {
            if (Auth::check()) {
               
                $id = Auth::id();
                $user = User::Find($id);
                $customers = Customer::All();

                return view('customers.index', ['customers' => $customers]);  
                    
        }
        return redirect()->intended('/home');
    
        }
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            return view ('customers.create');
        }
        return redirect()->intended('/home');
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            //add following if asked for verification
            
                    $this->validate($request,[
                        'name'=>'required',
                        'email'=>'required',
                        'phone'=>'required',
                        ]
                    );
            //
                            
                    $customer = new Customer();
                    $id = Auth::id(); //the id of the current user
                    $customer->name = $request->name;
                    $customer->email = $request->email;
                    $customer->phone = $request->phone;
                    $customer->user_id = $id;
                    $customer->status = 0;
                    $customer->save();
                    return redirect('customers');  
                }
                return redirect()->intended('/home');
                 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        if (Auth::check()) {
        $customer = Customer::find($id);
            if (Gate::denies('salesrep')) {
                return view('customers.edit', compact('customer'));
            }
       
        if(Auth::id() !== $customer->user_id) {
            abort(403,"Sorry, you do not hold permission to edit this customer");
        }
           
        return view('customers.edit', compact('customer'));
            }
        return redirect()->intended('/home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
                
       if (Auth::check()) {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required',
            ]
        );

       $customer = Customer::find($id);
       $customer->update($request->except(['_token']));
  
   
           return redirect('customers');           
       }
    return redirect()->intended('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check()) {
            if (Gate::denies('manager')) {
                abort(403,"You are not allowed to delete customers");
           }
            $customer = Customer::find($id);
            $customer->delete();
            return redirect('customers')->with('alert', 'Customer has been deleted');;
            }
        return redirect()->intended('/home');
    }
    public function deal($id)
    {
        if (Auth::check()) {
           
        $customer = Customer::find($id);            
        $customer->status = 1; 
    
        $customer->save();
        return redirect('customers')->with('alert', 'Congrats on closing a deal! Your prosper is now a customer ');    
        }
    return redirect()->intended('/home');
    }
}
