@extends('layouts.app')
@section('content')
<h1>Edit Customer Information</h1>
<form method = 'POST' action = "{{action('CustomerController@update' , $customer->id)}}">
@method('PATCH')
@csrf

<div class = "form-group">
<label for = "name">Name</label>
<input type = "text" class= "form-control" name = "name" value = "{{$customer->name}}">

<label for = "email">E-mail Address</label>
<input type = "email" class= "form-control" name = "email" value = "{{$customer->email}}">
<label for = "phone">Phone Number</label>
<input type = "phone" class= "form-control" name = "phone" value = "{{$customer->phone}}">
</div>

<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit" value = "Save" >
</div>


</form>

<!-- add following if asked for verification -->
@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



@endsection